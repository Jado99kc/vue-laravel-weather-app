<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Zttp\Zttp;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/currentWeather', function () {
    $apikey = config('services.openweatherapi.key');
    $lat = request('lat');
    $long = request('long');
    $response = Zttp::get("http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&appid=$apikey&units=metric");
    return $response->json();
});

Route::get('/forecast', function () {
    $apikey = config('services.openweatherapi.key');
    $lat = request('lat');
    $long = request('long');
    $response = Zttp::get("http://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$long&exclude={minutely,hourly}&appid=$apikey&units=metric");
    return $response->json();
});
